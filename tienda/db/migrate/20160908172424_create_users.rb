class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :user_name
      t.string :email
      t.string :clave
      t.references :city, index: true, foreign_key: true
      t.string :addres
      t.date :birthday

      t.timestamps null: false
    end
  end
end
