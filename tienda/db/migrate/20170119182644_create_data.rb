class CreateData < ActiveRecord::Migration
  def change
    create_table :data do |t|
      t.references :store, index: true, foreign_key: true
      t.references :marca, index: true, foreign_key: true
      t.references :tipo, index: true, foreign_key: true
      t.string :url_section
      t.string :path_paginator
      t.string :path_listado_productos
      t.string :path_producto_description
      t.string :path_producto_referencia
      t.string :path_producto_precio
      t.string :path_producto_imagen_principal
      t.string :path_producto_url_detalle
      t.string :path_producto_imagenes
      t.string :path_producto_imagenes
      t.string :path_producto_detalle_description
      t.string :path_producto_detalle_referencia

      t.timestamps null: false
    end
  end
end
