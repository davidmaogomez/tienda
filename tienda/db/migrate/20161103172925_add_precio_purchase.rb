class AddPrecioPurchase < ActiveRecord::Migration
  def change
    add_column :purchases, :valor, :float, :default => 0
  end
end
