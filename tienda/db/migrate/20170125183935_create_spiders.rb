class CreateSpiders < ActiveRecord::Migration
  def change
    create_table :spiders do |t|
      t.references :store, index: true, foreign_key: true
      t.references :marca, index: true, foreign_key: true
      t.references :tipo, index: true, foreign_key: true
      t.string :url_seccion
      t.string :path_listado_productos

      t.timestamps null: false
    end
  end
end
