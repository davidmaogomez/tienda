class AddChangePriceProducto < ActiveRecord::Migration
  def change
  	add_column :productos, :cambio_precio, :boolean, :default => false
  	add_column :productos, :cambio_precio_porcentaje, :float, :default => 0
  end
end
