class CreateMarcas < ActiveRecord::Migration
  def change
    create_table :marcas do |t|
      t.string :nombre
      t.string :url_imagen
      t.string :url

      t.timestamps null: false
    end
  end
end
