class AddValorCompraPurchase < ActiveRecord::Migration
  def change
    add_column :payments, :valor_compra, :float, :default => 0
  end
end
