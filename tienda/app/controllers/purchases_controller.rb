class PurchasesController < ApplicationController
  before_action :set_purchase, only: [:show, :edit, :update, :destroy]

  # GET /purchases
  # GET /purchases.json
  def panel_admin
    @tipos = Tipo.where(:id => Producto.select(:tipo_id).all)    
    @payments = Payment.where(:estado_id => 4)    
  end
  def mis_compras
    @tipos = Tipo.where(:id => Producto.select(:tipo_id).all)    
    @payments = Payment.where(:user_id => current_user.id, :estado_id => 4)
  end
  
  def admin_ventas
    @tipos = Tipo.where(:id => Producto.select(:tipo_id).all)    
    @compras = Payment.all
    @payments = {}
    Payment.all.group(:agrupador).count.each do |agrupador, cantidad|
      @payments[agrupador] = Payment.where(:agrupador => agrupador)
    end
  end    
  
  def crear_pago
    @tipos = Tipo.where(:id => Producto.select(:tipo_id).all)    
    constante = 200
    @purchases = Purchase.where(:user_id => current_user.id, :pago => false, :agrupador => 0)
    if @purchases.count > 0
      payment = Payment.create(:estado_id => 1, :user_id => current_user.id, :valor_compra => @purchases.sum(:valor))
      identificador_purchase = payment.id+constante
      @purchases.each do |purchase|
        purchase.agrupador = identificador_purchase
        purchase.save
      end 
    else
      @purchases = Purchase.where(:user_id => current_user.id, :pago => false, :estado_id => 6)
      if @purchases.count > 0
        payment = Payment.create(:estado_id => 1, :user_id => current_user.id, :valor_compra => @purchases.sum(:valor))
        identificador_purchase = payment.id+constante
        @purchases.each do |purchase|
          purchase.agrupador = identificador_purchase
          purchase.save
        end         
      end
    end
    @api_key= '4Vj8eK4rloUd272L48hsrarnUA'
    @merchant_id = '508029'
    @account_id = '512321'
    @reference_code = "TestPayU#{identificador_purchase}"
    @amount = '3' #@amount = @purchases.sum(:valor)
    @currency_value = 'USD'# @currency_value = 'COP'    
    cadena_combinacion = "#{@api_key}~#{@merchant_id}~#{  @reference_code}~#{@amount}~#{@currency_value}"
    @md5_value = Digest::MD5.hexdigest(cadena_combinacion)
    @buyer_email_value = 'damagoza@gmail.com'
    @respuesta_value = "https://tienda-damagoza.c9users.io#{respuesta_path}"
    @confirmacion_value = "https://tienda-damagoza.c9users.io#{confirmacion_path}"   
    payment.reference_sale = @reference_code
    payment.save
  end
  
  def add_to_card
    @tipos = Tipo.where(:id => Producto.select(:tipo_id).all)    
    if permiso(current_user, 3)
      @producto = Producto.find(params[:producto_id])
      @purchase = Purchase.create(:user_id => current_user.id, :producto_id => @producto.id, :pago => false, :valor => @producto.precio_venta, :moneda => @producto.moneda, :valor_divisa => @producto.divisa) 
      puts "---------------> #{@purchase.to_json}"
      redirect_to show_card_path      
    end
  end

  def show_card
    @tipos = Tipo.where(:id => Producto.select(:tipo_id).all)    
    url = "https://tienda-damagoza.c9users.io/"
    @respuesta = url + respuesta_path
    @confirmacion = url + confirmacion_path
    if permiso(current_user, 3)
      @purchases = Purchase.where(:user_id => current_user.id, :pago => false)
    end
  end

  def index
    if permiso(current_user, 3)
      @purchases = Purchase.where(:user_id => current_user.id)
      if not @purchases.count > 0
        redirect_to root_path
      end
    end  
  end

  # GET /purchases/1
  # GET /purchases/1.json

  # GET /purchases/new
  def new
    if permiso(current_user, 1)
        @purchase = Purchase.new
    end
  end

  # GET /purchases/1/edit
  def edit
  end

  # POST /purchases
  # POST /purchases.json
  def create
    if permiso(current_user, 3)
      @purchase = Purchase.new(purchase_params)
      respond_to do |format|
        if @purchase.save
          format.html { redirect_to @purchase, notice: 'Purchase was successfully created.' }
          format.json { render :show, status: :created, location: @purchase }
        else
          format.html { render :new }
          format.json { render json: @purchase.errors, status: :unprocessable_entity }
        end
      end      
    end
  end

  # PATCH/PUT /purchases/1
  # PATCH/PUT /purchases/1.json
  def update
    if permiso(current_user, 3)
      if @purchase.update(purchase_params)
        redirect_to show_card_path
      else
        
      end      
    end    
  end

  # DELETE /purchases/1
  # DELETE /purchases/1.json
  def destroy
    if permiso(current_user, 3)
      payment = Payment.where(:id => @purchase.id)
      if payment.count == 1
        @purchase.destroy
        payment.delete
      else
        @purchase.destroy
      end
      redirect_to show_card_path      
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_purchase
      @purchase = Purchase.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def purchase_params
      params.require(:purchase).permit(:user_id, :producto_id, :estado, :cantidad)
    end
end
