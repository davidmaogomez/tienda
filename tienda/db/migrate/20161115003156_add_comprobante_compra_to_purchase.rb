class AddComprobanteCompraToPurchase < ActiveRecord::Migration
  def change
    add_column :purchases, :comprobante, :string, :default => ''
  end
end
