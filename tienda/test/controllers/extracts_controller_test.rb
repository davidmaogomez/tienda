require 'test_helper'

class ExtractsControllerTest < ActionController::TestCase
  setup do
    @extract = extracts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:extracts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create extract" do
    assert_difference('Extract.count') do
      post :create, extract: { marca_id: @extract.marca_id, path_listado_productos: @extract.path_listado_productos, path_paginator: @extract.path_paginator, path_producto_description: @extract.path_producto_description, path_producto_detalle_description: @extract.path_producto_detalle_description, path_producto_detalle_referencia: @extract.path_producto_detalle_referencia, path_producto_imagen_principal: @extract.path_producto_imagen_principal, path_producto_imagenes: @extract.path_producto_imagenes, path_producto_imagenes: @extract.path_producto_imagenes, path_producto_precio: @extract.path_producto_precio, path_producto_referencia: @extract.path_producto_referencia, path_producto_url_detalle: @extract.path_producto_url_detalle, store_id: @extract.store_id, tipo_id: @extract.tipo_id, url_section: @extract.url_section }
    end

    assert_redirected_to extract_path(assigns(:extract))
  end

  test "should show extract" do
    get :show, id: @extract
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @extract
    assert_response :success
  end

  test "should update extract" do
    patch :update, id: @extract, extract: { marca_id: @extract.marca_id, path_listado_productos: @extract.path_listado_productos, path_paginator: @extract.path_paginator, path_producto_description: @extract.path_producto_description, path_producto_detalle_description: @extract.path_producto_detalle_description, path_producto_detalle_referencia: @extract.path_producto_detalle_referencia, path_producto_imagen_principal: @extract.path_producto_imagen_principal, path_producto_imagenes: @extract.path_producto_imagenes, path_producto_imagenes: @extract.path_producto_imagenes, path_producto_precio: @extract.path_producto_precio, path_producto_referencia: @extract.path_producto_referencia, path_producto_url_detalle: @extract.path_producto_url_detalle, store_id: @extract.store_id, tipo_id: @extract.tipo_id, url_section: @extract.url_section }
    assert_redirected_to extract_path(assigns(:extract))
  end

  test "should destroy extract" do
    assert_difference('Extract.count', -1) do
      delete :destroy, id: @extract
    end

    assert_redirected_to extracts_path
  end
end
