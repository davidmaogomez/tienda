class AddEstadoPayment < ActiveRecord::Migration
  def change
    add_reference :payments, :estado, index: true, foreign_key: true
  end
end
