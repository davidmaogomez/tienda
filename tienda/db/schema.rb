# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170212172238) do

  create_table "carts", force: :cascade do |t|
    t.integer  "producto_id"
    t.integer  "user_id"
    t.integer  "payment_id",  default: 0
    t.integer  "estado_id",   default: 1
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "quantity",    default: 0
  end

  add_index "carts", ["estado_id"], name: "index_carts_on_estado_id"
  add_index "carts", ["payment_id"], name: "index_carts_on_payment_id"
  add_index "carts", ["producto_id"], name: "index_carts_on_producto_id"
  add_index "carts", ["user_id"], name: "index_carts_on_user_id"

  create_table "changes", force: :cascade do |t|
    t.integer  "producto_id"
    t.float    "precio"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "changes", ["producto_id"], name: "index_changes_on_producto_id"

  create_table "cities", force: :cascade do |t|
    t.string   "name"
    t.integer  "country_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "cities", ["country_id"], name: "index_cities_on_country_id"

  create_table "countries", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "moneda",        default: ""
    t.string   "code_currency", default: ""
    t.integer  "currency_id"
  end

  add_index "countries", ["currency_id"], name: "index_countries_on_currency_id"

  create_table "currencies", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.float    "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "data", force: :cascade do |t|
    t.integer  "store_id"
    t.integer  "marca_id"
    t.integer  "tipo_id"
    t.string   "url_section"
    t.string   "path_paginator"
    t.string   "path_listado_productos"
    t.string   "path_producto_description"
    t.string   "path_producto_referencia"
    t.string   "path_producto_precio"
    t.string   "path_producto_imagen_principal"
    t.string   "path_producto_url_detalle"
    t.string   "path_producto_imagenes"
    t.string   "path_producto_detalle_description"
    t.string   "path_producto_detalle_referencia"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "data", ["marca_id"], name: "index_data_on_marca_id"
  add_index "data", ["store_id"], name: "index_data_on_store_id"
  add_index "data", ["tipo_id"], name: "index_data_on_tipo_id"

  create_table "estados", force: :cascade do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "marca_tipos", force: :cascade do |t|
    t.integer  "tipo_id"
    t.integer  "marca_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "marca_tipos", ["marca_id"], name: "index_marca_tipos_on_marca_id"
  add_index "marca_tipos", ["tipo_id"], name: "index_marca_tipos_on_tipo_id"

  create_table "marcas", force: :cascade do |t|
    t.string   "nombre"
    t.string   "url_imagen"
    t.string   "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "payments", force: :cascade do |t|
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "estado_id"
    t.string   "reference_sale"
    t.integer  "user_id"
    t.string   "confirmacion",   default: ""
    t.string   "resultado",      default: ""
    t.float    "valor_compra",   default: 0.0
  end

  add_index "payments", ["estado_id"], name: "index_payments_on_estado_id"
  add_index "payments", ["user_id"], name: "index_payments_on_user_id"

  create_table "productos", force: :cascade do |t|
    t.string   "imagen",                   default: "no_image"
    t.string   "descripcion",              default: "Este producto no tiene descripcion"
    t.float    "precio_min",               default: 0.0
    t.float    "precio_max",               default: 0.0
    t.float    "precio_envio",             default: 0.0
    t.float    "porcentaje_ganancia",      default: 0.0
    t.float    "precio_venta",             default: 0.0
    t.string   "moneda",                   default: "$"
    t.string   "url",                      default: "no_url"
    t.datetime "created_at",                                                              null: false
    t.datetime "updated_at",                                                              null: false
    t.integer  "marca_id",                 default: -1
    t.string   "detalle",                  default: ""
    t.boolean  "cambio_precio",            default: false
    t.float    "cambio_precio_porcentaje", default: 0.0
    t.integer  "tipo_id"
    t.float    "grv",                      default: 0.0
    t.float    "iva",                      default: 0.0
    t.float    "ganancia"
    t.float    "precio_base"
    t.boolean  "nuevo",                    default: true
    t.float    "precio_sin_iva_grv",       default: 0.0
    t.float    "precio_anterior",          default: 0.0
    t.string   "reference_sale"
    t.integer  "marca_tipo_id"
    t.integer  "currency_id"
  end

  add_index "productos", ["currency_id"], name: "index_productos_on_currency_id"
  add_index "productos", ["marca_tipo_id"], name: "index_productos_on_marca_tipo_id"
  add_index "productos", ["tipo_id"], name: "index_productos_on_tipo_id"

  create_table "purchases", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "producto_id"
    t.boolean  "pago"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "cantidad",      default: 1
    t.float    "valor",         default: 0.0
    t.string   "moneda",        default: ""
    t.float    "valor_divisa",  default: 0.0
    t.integer  "agrupador",     default: 0
    t.integer  "estado_id",     default: 1
    t.string   "comprobante",   default: ""
    t.string   "numero_pedido", default: ""
  end

  add_index "purchases", ["estado_id"], name: "index_purchases_on_estado_id"
  add_index "purchases", ["producto_id"], name: "index_purchases_on_producto_id"
  add_index "purchases", ["user_id"], name: "index_purchases_on_user_id"

  create_table "scrapies", force: :cascade do |t|
    t.integer  "marca_id"
    t.integer  "tipo_id"
    t.string   "imagen_path"
    t.string   "descripcion_path"
    t.string   "precio_path"
    t.string   "moneda_path"
    t.string   "url_path"
    t.string   "detalle_path"
    t.string   "detalle_title_path"
    t.string   "detalle_des_path"
    t.string   "detalle_general_path"
    t.string   "detail_gallery_path"
    t.string   "imagenes_path"
    t.string   "imag_path"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "store_id"
    t.string   "url",                  default: ""
    t.string   "productos_path",       default: ""
    t.string   "imagen_atributos",     default: ""
    t.string   "url_atributos",        default: ""
    t.string   "imag_atributos",       default: ""
    t.string   "next_page_path",       default: ""
    t.string   "dominio",              default: ""
    t.integer  "hasta_nivel",          default: 0
    t.integer  "desde_nivel",          default: 1
  end

  add_index "scrapies", ["marca_id"], name: "index_scrapies_on_marca_id"
  add_index "scrapies", ["store_id"], name: "index_scrapies_on_store_id"
  add_index "scrapies", ["tipo_id"], name: "index_scrapies_on_tipo_id"

  create_table "spiders", force: :cascade do |t|
    t.integer  "store_id"
    t.integer  "marca_id"
    t.integer  "tipo_id"
    t.string   "url_seccion"
    t.string   "path_listado_productos"
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.string   "path_paginador",                    default: ""
    t.string   "path_producto_description",         default: ""
    t.string   "path_producto_referencia",          default: ""
    t.string   "path_producto_precio",              default: ""
    t.string   "path_producto_imagen_principal",    default: ""
    t.string   "path_producto_url_detalle",         default: ""
    t.string   "path_producto_imagenes",            default: ""
    t.string   "path_producto_detalle_description", default: ""
    t.string   "path_producto_detalle_referencia",  default: ""
    t.string   "path_precio_detalle",               default: ""
    t.string   "pat_especificaciones",              default: ""
    t.integer  "currency_id"
    t.integer  "marca_tipo_id"
    t.boolean  "funcional",                         default: false
  end

  add_index "spiders", ["currency_id"], name: "index_spiders_on_currency_id"
  add_index "spiders", ["marca_id"], name: "index_spiders_on_marca_id"
  add_index "spiders", ["marca_tipo_id"], name: "index_spiders_on_marca_tipo_id"
  add_index "spiders", ["store_id"], name: "index_spiders_on_store_id"
  add_index "spiders", ["tipo_id"], name: "index_spiders_on_tipo_id"

  create_table "stores", force: :cascade do |t|
    t.string   "name"
    t.string   "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tipos", force: :cascade do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "user_name"
    t.string   "email"
    t.string   "clave"
    t.integer  "city_id"
    t.string   "addres"
    t.date     "birthday"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "nivel",      default: 3
  end

  add_index "users", ["city_id"], name: "index_users_on_city_id"

end
