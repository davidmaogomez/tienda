class AddConfirmacionToPayment < ActiveRecord::Migration
  def change
    add_column :purchases, :confirmacion, :string, :default => ""
  end
end
