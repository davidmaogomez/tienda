class ChangeDefaultCantidadPurchase < ActiveRecord::Migration
  def change
    change_column :purchases, :cantidad, :integer, :default => 1
  end
end
