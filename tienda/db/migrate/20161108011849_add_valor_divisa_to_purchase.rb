class AddValorDivisaToPurchase < ActiveRecord::Migration
  def change
  	add_column :purchases, :valor_divisa, :float, :default => 0
  end
end
