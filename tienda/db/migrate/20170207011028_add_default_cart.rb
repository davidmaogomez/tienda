class AddDefaultCart < ActiveRecord::Migration
  def change
	change_column :carts, :payment_id, :integer, :default => 0
        change_column :carts, :estado_id, :integer, :default => 1
  end
end
