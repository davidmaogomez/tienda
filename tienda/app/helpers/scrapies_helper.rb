module ScrapiesHelper
	def extract_items(object_site_html, scrapy)
		puts "----------------------------------extract_items----------------------------------"
		productos = object_site_html.css(scrapy.productos_path)	
		puts "productos = #{productos.count}"	
		productos.each do |producto|
		tipo = scrapy.tipo_id
		imagen = ""
		scrapy.imagen_atributos.split(';').each do |atributo|
			if "#{producto.css(scrapy.imagen_path).attr(atributo)}".length > 0
				imagen = "#{producto.css('div.img a img').attr(atributo)}".gsub(/[\n]/, '')
			end          
		end
		descripcion = producto.css(scrapy.descripcion_path)
		precio = producto.css(scrapy.descripcion_path)
		url  = "http:#{producto.css(scrapy.url_path).attr('href')}"
		page_detalle = Nokogiri::HTML(open(url).read)
		detalle = page_detalle.css(scrapy.detalle_path)
		detalle_general = ""
		detalle.each do |detalle_especifico|
			detalle_title = detalle_especifico.css(scrapy.detalle_title_path).text
			detalle_des = detalle_especifico.css(scrapy.detalle_des_path).text				
			detalle_general += "#{detalle_title} #{detalle_des} |"
		end				
		detail_gallery = page_detalle.css(scrapy.detail_gallery_path)
		imagenes = detail_gallery.css(scrapy.imagenes_path)
		if imagenes.count > 0
		  imagenes.each do |bloque|
		    imag = ""
		    scrapy.imag_atributos.split(';').each do |atributo|
				if "#{bloque.css(scrapy.imag_path).attr(atributo)}".length > 0
					imag = "#{bloque.css(scrapy.imag_path).attr(atributo)}".gsub(/[\n]/, '')
				end	            
		    end
			if imag != ""
				imagen += "|#{imag}"							
			end            
		  end
		else
		end
		imagen += "|"		                
		end		
	end
end
