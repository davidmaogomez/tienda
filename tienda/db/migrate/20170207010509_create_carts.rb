class CreateCarts < ActiveRecord::Migration
  def change
    create_table :carts do |t|
      t.references :producto, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.references :payment, index: true, foreign_key: true
      t.references :estado, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
