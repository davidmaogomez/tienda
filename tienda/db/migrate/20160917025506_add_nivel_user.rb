class AddNivelUser < ActiveRecord::Migration
  def change
    add_column :users, :nivel, :integer, :default => 3
  end
end
