class RemoveColumPayment < ActiveRecord::Migration
  def change
    remove_column :payments, :agrupador, :integer
    add_column :payments, :productos, :string
  end
end
