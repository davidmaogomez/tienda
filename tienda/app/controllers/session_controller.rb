class SessionController < ApplicationController
  def new
    @tipos = Tipo.where(:id => Producto.select(:tipo_id).all)  
  end
  
  def create
    puts "***Entre a create email #{params[:session][:email]}, clave #{params[:session][:clave]} ***"
    user = User.find_by email: params[:session][:email]
    if user
      if user.clave ==  params[:session][:clave]
        log_in user
        redirect_to root_path        
      else
        flash[:clave] = "Clave incorrecta"
        redirect_to login_path
      end
    else
      flash[:email] = "E-mail incorrecto"
      redirect_to login_path
    end
  end
  
  def destroy
    log_out
    redirect_to root_url
  end
end


