/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scrapy;
import java.io.IOException;

import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
/**
 *
 * @author davidmao
 */
public class Scrapy {
    String url = "";
    Scrapy(String url) {
      this.url = url;          
    }
   
    public int getStatusConnectionCode(String url){
        int statusCode = 0;
        Response response;
        try {
            response = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(100000).ignoreHttpErrors(true).execute();
            statusCode = response.statusCode();
        } catch (Exception e) {
            System.out.println("Excepción al obtener el Status Code: " + e.getMessage());
        }        
        return statusCode;
    }
    
    public static Document getHtmlDocument(String url) {
        Document doc = null;
        try {
            doc = (Document) Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(100000).get();
        } catch (IOException ex) {
            System.out.println("Excepción al obtener el HTML de la página" + ex.getMessage());
        }
        return doc;
    }   
    
    public static Elements getElementsHtmlDocument(Document document, String direccion){
        Elements elementos = document.select(direccion);
        return elementos;
    }
    
}
