class AddDefaultValueAgrupador < ActiveRecord::Migration
  def change
    change_column :payments, :agrupador, :integer, :default => 0
  end
end
