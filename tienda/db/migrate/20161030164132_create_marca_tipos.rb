class CreateMarcaTipos < ActiveRecord::Migration
  def change
    create_table :marca_tipos do |t|
      t.references :tipo, index: true, foreign_key: true
      t.references :marca, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
