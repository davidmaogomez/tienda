class RemoveProductosToPayment < ActiveRecord::Migration
  def change
    remove_column :payments, :productos, :string
  end
end
