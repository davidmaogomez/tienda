class Estado < ActiveRecord::Base
    has_many :payments
    has_many :carts
end
