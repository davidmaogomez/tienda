class AddAtributosToPath < ActiveRecord::Migration
  def change
    add_column :scrapies, :imagen_atributos, :string, :default => ""
    add_column :scrapies, :url_atributos, :string, :default => ""
  end
end
