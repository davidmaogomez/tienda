class AddCurrencyAndCodeCurrencyToCountry < ActiveRecord::Migration
  def change
    add_column :countries, :currency, :string, :default => ''
    add_column :countries, :code_currency, :string, :default => ''
  end
end
