class AddUrlToScrapy < ActiveRecord::Migration
  def change
  	add_column :scrapies, :url, :string, :default => ''
  end
end
