class AddNumeroPedidoToPurchase < ActiveRecord::Migration
  def change
    add_column :purchases, :numero_pedido, :string, :default => ""
  end
end
