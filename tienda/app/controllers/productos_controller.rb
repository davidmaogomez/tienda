class ProductosController < ApplicationController
  before_action :set_producto, only: [:show, :edit, :update, :destroy]

  # GET /productos
  # GET /productos.json
  def orden
    puts " Me llegaron #{params[:productos].count}"
  end

  def productos_por_tipo
    current_tipo = params[:tipo_id]
    @productos = Producto.where(:tipo_id => current_tipo).order(:precio_min => params[:orden]).paginate(:page => params[:page], :per_page => 18)
    @tipos = Tipo.where(:id => Producto.select(:tipo_id).all)
    @marcas = Marca.where(:id => @productos.select(:marca_id).all)
  end

  def productos_por_marca
    current_marca = params[:marca_id]
    @productos = Producto.where(:marca_id => current_marca).order(:precio_min => params[:orden]).paginate(:page => params[:page], :per_page => 18)
    @tipos = Tipo.where(:id => Producto.select(:tipo_id).all)
    @marcas = Marca.where(:id => @productos.select(:marca_id).all)
    puts "tipos = #{@tipos.count}"
    puts "productos = #{@productos.count}"
    puts "marcas = #{@marcas.count}"
  end

  def productos_por_marca_tipo  
    @current_marca = params[:marca_id]
    @current_tipo = params[:tipo_id]
    productos_gon = productos_tag
    @tipos = Tipo.where(:id => Producto.select(:tipo_id).all)
    @marcas_tipo = MarcaTipo.where(:tipo_id => params[:tipo_id], :marca_id => Producto.select(:marca_id).all)
    if params[:marca_id] == nil && params[:tipo_id] != nil
      params[:marca_id] = MarcaTipo.where(:tipo_id => params[:tipo_id]).first.marca_id
    end
    @productos = Producto.where(:marca_id => params[:marca_id], :tipo_id => params[:tipo_id]).paginate(:page => params[:page], :per_page => 18).order(:precio_venta => params[:orden])
    if params[:cambio_precio] != nil
      @productos = @productos.where(:cambio_precio => params[:cambio_precio]).where("cambio_precio_porcentaje #{params[:signo]} ?", 0)
    end      
    if params[:nuevo] != nil
      @productos = @productos.where(:nuevo => params[:nuevo])
    end          
    @marca = params[:marca_id]
  end

  def find_producto
    @tipos = Tipo.where(:id => Producto.select(:tipo_id).all)
    termino = "%#{params[:searchProducto]}%"
    precio = params[:precio].to_f / 3304.81
    @productos = Producto.all
    if termino != ""
      puts "Linea 51"
      if Producto.exists?(['descripcion LIKE ? OR detalle LIKE ?', termino, termino])
        puts "Linea 51"
        @productos = Producto.where(['descripcion LIKE ? OR detalle LIKE ?', termino, termino]).paginate(:page => params[:page], :per_page => 18)    
      else      
        puts "Linea 54"
        redirect_to root_path
        flash[:notice] = "Nada por aca"
      end                  
    else      
    end     
    @marcas = Marca.where(:id => @productos.select(:marca_id).all)
  end

  def index
    @tipos = Tipo.where(:id => Producto.select(:tipo_id).all)    
    @productos = Producto.all.paginate(:page => params[:page], :per_page => 18).order(:precio_venta => 'desc') 
    # if params[:orden] == nil
    #   puts " ----------------> #1 #{params[:orden]}"      
    #   @productos = Producto.all.paginate(:page => params[:page], :per_page => 18).order(:precio_venta => 'desc')      
    # else
    #   puts " ----------------> #2 #{params[:orden].class}"
    #   @productos = Producto.all.paginate(:page => params[:page], :per_page => 18).order(:precio_venta => params[:orden])    
    # end  
    # if params[:cambio_precio] != nil
    #   @productos = @productos.where(:cambio_precio => params[:cambio_precio]).where("cambio_precio_porcentaje #{params[:signo]} ?", 0)
    # end  
    # if params[:nuevo] != nil
    #   @productos = @productos.where(:nuevo => params[:nuevo])
    # end      
    productos_gon = productos_tag    
    @marcas = Marca.all
    # @marcas = Marca.where(:id => @productos.select(:marca_id))
  end

  # GET /productos/1
  # GET /productos/1.json
  def show
    @tipos = Tipo.where(:id => Producto.select(:tipo_id).all)
    productos_gon = productos_tag
    @diccionario = diccionario
  end

  # GET /productos/new
  def new
    @producto = Producto.new
  end

  # GET /productos/1/edit
  def edit
  end

  # POST /productos
  # POST /productos.json
  def create
    @producto = Producto.new(producto_params)

    respond_to do |format|
      if @producto.save
        format.html { redirect_to @producto, notice: 'Producto was successfully created.' }
        format.json { render :show, status: :created, location: @producto }
      else
        format.html { render :new }
        format.json { render json: @producto.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /productos/1
  # PATCH/PUT /productos/1.json
  def update
    respond_to do |format|
      if @producto.update(producto_params)
        format.html { redirect_to @producto, notice: 'Producto was successfully updated.' }
        format.json { render :show, status: :ok, location: @producto }
      else
        format.html { render :edit }
        format.json { render json: @producto.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /productos/1
  # DELETE /productos/1.json
  def destroy
    @producto.destroy
    respond_to do |format|
      format.html { redirect_to productos_url, notice: 'Producto was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_producto
      @producto = Producto.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def producto_params
      params.require(:producto).permit(:imagen, :descripcion, :precio_min, :precio_max, :precio_envio, :porcentaje_ganancia, :precio_venta, :moneda, :url)
    end
end
