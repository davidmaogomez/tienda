class SpidersController < ApplicationController
  before_action :set_spider, only: [:show, :edit, :update, :destroy]
  include SpidersHelper

  def asignar_spider
    origen = Spider.find(params[:origen])
    destino = Spider.find(params[:destino])
    destino.path_listado_productos = origen.path_listado_productos
    destino.path_precio_detalle = origen.path_precio_detalle
    destino.path_paginador = origen.path_paginador
    destino.path_producto_description = origen.path_producto_description
    destino.path_producto_referencia = origen.path_producto_referencia
    destino.path_producto_precio = origen.path_producto_precio
    destino.path_producto_imagen_principal = origen.path_producto_imagen_principal
    destino.path_producto_url_detalle = origen.path_producto_url_detalle
    destino.path_producto_imagenes = origen.path_producto_imagenes
    destino.path_producto_detalle_description = origen.path_producto_detalle_description
    destino.path_producto_detalle_referencia = origen.path_producto_detalle_referencia
    destino.path_precio_detalle = origen.path_precio_detalle
    destino.pat_especificaciones = origen.pat_especificaciones
    destino.currency_id = origen.currency_id
    destino.save
    redirect_to destino
  end

  def traer_data
    permiso(current_user, 1)
    @spider = Spider.find(params[:id])
    ruta_folder = "../data/#{@spider.id}_#{@spider.store.name}_#{@spider.marca.nombre}_#{@spider.tipo.nombre}"
    traer_data_site(ruta_folder, @spider)
    redirect_to spiders_path      
  end

  def extract
    puts "entro a extract"
    permiso(current_user, 1)
    extraer = false
    @spider = Spider.find(params[:id])
    nombre_folder = "../data/#{@spider.id}_#{@spider.store.name}_#{@spider.marca.nombre}_#{@spider.tipo.nombre}"
    ruta_folder = Dir.glob("#{nombre_folder}")
    if File.exist?(ruta_folder[0]) == false
      extraer = traer_data_site(ruta_folder, @spider)
    else
      extraer = true
    end

    if extraer == true
      puts "extraer = true"
      last_folder = Dir.glob("#{ruta_folder[0]}/*").last
      archivos_folder = Dir.glob("#{last_folder}/*")
      puts last_folder
      puts archivos_folder
      archivos_folder.count.times do |time|
        documento = Nokogiri::HTML(File.read(File.open(archivos_folder[time], "rb")))
        productos = documento.css(@spider.path_listado_productos)
        cont = 1
        productos.each do |producto|
          atributes_path = ["#{@spider.path_producto_imagen_principal}", "#{@spider.path_producto_description}", 
                            "#{@spider.path_producto_precio}", "#{@spider.path_producto_url_detalle}"]
          atributes_info = extract_info(producto, atributes_path)
          #imagen = atributes_info[0].split("|")[0] 
          imagen = ""         
          descripcion = atributes_info[1].split("|")[0]          
          precio_min = atributes_info[2].split("|")[0].to_f    
          precio_max = 0.0 
          precio_envio = 0.0
          porcentaje_ganancia = 0.0  
          if precio_max == 0.0
            precio_max = precio_min
          end   

          url = atributes_info[3].split("|")[0]  

          if url != nil
            atributes_path.count.times do |indice|
              if indice == 3
                # url = "#{atributes_info[indice].split("|")[0]}"
                if url.length() > 1
                  if Net::HTTP.get_response(URI.parse(URI.encode(url.strip))).code == '200'
                    contenido = Nokogiri::HTML(open(URI.encode(url.strip)))
                    #sleep 1
                    if contenido.to_s.length > 0            
                      puts "13- contenido > 0"
                      titulo = ""
                      detalle_general = ""
                      imagenes = "|"
                      @spider.path_producto_detalle_referencia.split(",").count.times do |time|                      
                        titulo += extract_tag(contenido, @spider.path_producto_detalle_referencia.split(",")[time])
                      end 
                      puts "14- titulo #{titulo} #{titulo.class}"                   
                      @spider.pat_especificaciones.split(",").count.times do |time|                      
                        detalle_general += "#{extract_tag(contenido, @spider.pat_especificaciones.split(",")[time])}"
                      end     
                      puts "15- detalle_general #{detalle_general} #{detalle_general.class}"                                         
                      lista_imagenes = contenido.css(@spider.path_producto_imagenes.split("&")[0])
                      lista_imagenes.each do |elemento|                        
                        imagen += "#{extract_tag(elemento, @spider.path_producto_imagenes.split("&")[1])}|"
                      end            
                      puts "16- imagen #{imagen} #{imagen.class}"                                                               
                      imagen += "|"
                      precio = extract_tag(contenido, @spider.path_precio_detalle)  
                      puts "17- precio #{precio} #{precio.class}"                                                               
                      if precio.class == Nokogiri::XML::Attr or precio != nil
                        puts "18- precio index = #{precio_min}, precio detalle = #{precio}" 
                        precio = precio.to_s.to_f
                        if precio_min < precio
                          puts "18.1- entra"
                          precio_min = precio
                        else
                          puts "18.1 no entra"
                        end

                        if precio_min > 0.0
                          puts "19- precio_min #{precio_min} #{precio_min.class}"                                                               
                          puts "*********************************"   
                          precio_base = precio_max +  precio_envio  
                          iva = precio_base * 0.19.to_f
                          grv = (precio_envio + iva) * 0.05.to_f
                          comision_plataforma = precio_base * 0.03.to_f
                          ganancia = precio_base * 0.05.to_f
                          precio_venta = precio_base + iva + grv + comision_plataforma + ganancia
                          precio_sin_iva_grv = precio_base + comision_plataforma + ganancia
                          p = Producto.find_by_url(url)
                          puts "20- p #{p.class}"                                             
                          if p.class == NilClass
                           p = Producto.create(:imagen => imagen, :descripcion => titulo, :precio_min => precio_min, :precio_max => precio_max, :precio_envio => precio_envio, :porcentaje_ganancia => porcentaje_ganancia, :precio_venta => precio_venta, :precio_sin_iva_grv => precio_sin_iva_grv, :moneda => '$$', :url => url, :marca_id => @spider.marca_id, :detalle => detalle_general, :grv => grv, :iva => iva, :ganancia => ganancia, :precio_base => precio_base, :tipo_id => @spider.tipo_id, :currency_id => @spider.currency_id, :marca_tipo_id => @spider.marca_tipo_id)
                           Change.create(:producto_id => p.id, :precio => p.precio_venta)
                           puts 'Producto nuevo #{url}'
                          else
                           puts 'encontrado #{url}'
                           if p
                             p.nuevo=false
                             p.imagen=imagen
                             p.precio_sin_iva_grv=precio_sin_iva_grv
                             p.save
                             if p.precio_venta != precio_venta
                               puts 'cambio de precio '
                               p.cambio_precio=true
                               p.cambio_precio_porcentaje=((precio_venta*100)/p.precio_venta)-100
                               p.precio_min=precio_min
                               p.precio_max=precio_max
                               p.precio_envio=precio_envio
                               p.grv=grv
                               p.iva=iva
                               p.ganancia=ganancia
                               p.precio_base=precio_base
                               p.precio_sin_iva_grv=precio_sin_iva_grv
                               p.precio_anterior=p.precio_venta
                               p.precio_venta=precio_venta
                               p.save
                               Change.create(:producto_id => p.id, :precio => p.precio_venta)
                             else
                               p.cambio_precio=false
                               p.cambio_precio_porcentaje=0
                               p.save
                             end
                           else
                           end
                          end                 
                          atributes_path.clear
                          atributes_info.clear                                                                                        
                        end
                      end
                    end
                  end              
                end
              end
            end
            cont = cont + 1    
            puts "************************************************************************************************************************"               
          end               

        end                        

      end      
      extraer = false
    end

  end  

  # GET /spiders
  # GET /spiders.json
  def index
    permiso(current_user, 1)
    @spiders = Spider.all
  end

  # GET /spiders/1
  # GET /spiders/1.json
  def show
    permiso(current_user, 1)
  end

  # GET /spiders/new
  def new
    permiso(current_user, 1)
    @spider = Spider.new
    if @spider
      busqueda = MarcaTipo.where(:marca_id => @spider, :tipo_id => @spider)
      if busqueda.count > 0
        mt = MarcaTipo.create(:marca_id => @spider, :tipo_id => @spider)
        if mt
          @spider.marca_tipo_id = mt.id
          mt.save
        end        
      else
      end
    end
  end

  # GET /spiders/1/edit
  def edit
    permiso(current_user, 1)
    @spider_all = Spider.all
  end

  # POST /spiders
  # POST /spiders.json
  def create
    # # puts "spider_params\n #{spider_params}"
    @spider = Spider.new(spider_params)

    respond_to do |format|
      if @spider.save
        format.html { redirect_to @spider, notice: 'Spider was successfully created.' }
        format.json { render :show, status: :created, location: @spider }
      else
        format.html { render :new }
        format.json { render json: @spider.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /spiders/1
  # PATCH/PUT /spiders/1.json
  def update
    # # puts "spider_params\n #{spider_params}"    
    respond_to do |format|
      if @spider.update(spider_params)
        format.html { redirect_to @spider, notice: 'Spider was successfully updated.' }
        format.json { render :show, status: :ok, location: @spider }
      else
        format.html { render :edit }
        format.json { render json: @spider.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /spiders/1
  # DELETE /spiders/1.json
  def destroy
    @spider.destroy
    respond_to do |format|
      format.html { redirect_to spiders_url, notice: 'Spider was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_spider
      @spider = Spider.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def spider_params
      params.require(:spider).permit(:store_id, :marca_id, :tipo_id, :url_seccion, :path_listado_productos, :path_precio_detalle, :path_paginador, :path_producto_description, :path_producto_referencia, :path_producto_precio, :path_producto_imagen_principal, :path_producto_url, :path_producto_imagenes, :path_producto_detalle_description, :path_producto_detalle_referencia, :pat_especificaciones, :currency_id, :path_producto_url_detalle, :funcional)
    end
end

