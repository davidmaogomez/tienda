class RenameColumToSpider < ActiveRecord::Migration
  def change
    rename_column :spiders, :site_html, :path_paginador
  end
end
