class ValueDefaultProducto < ActiveRecord::Migration
  def change
  	change_column :productos, :imagen, :string, :default => 'no_image'
  	change_column :productos, :descripcion, :string, :default => 'Este producto no tiene descripcion'
  	change_column :productos, :precio_min, :float, :default => 0
  	change_column :productos, :precio_max, :float, :default => 0
  	change_column :productos, :precio_envio, :string, :default => 0
  	change_column :productos, :porcentaje_ganancia, :float, :default => 0
   	change_column :productos, :moneda, :string, :default => '$'
   	change_column :productos, :url, :string, :default => 'no_url' 
  end
end
