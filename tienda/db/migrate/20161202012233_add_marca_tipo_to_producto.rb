class AddMarcaTipoToProducto < ActiveRecord::Migration
  def change
  	add_reference :productos, :marca_tipo, index:true, foreign_key:true
  end
end
