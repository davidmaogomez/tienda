class ChangeColumnPrecioVenta < ActiveRecord::Migration
  def change
  	change_column :productos, :precio_venta, :float
  end
end
