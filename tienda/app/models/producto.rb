class Producto < ActiveRecord::Base	
	belongs_to :marca
	belongs_to :tipo
	has_many :change
	belongs_to :marca_tipo
	belongs_to :currency
	has_many :carts
	def divisa
		valor_divisa = 0
		case self.moneda
		when "€"
			valor_divisa= 3304.81
		when "$"
			valor_divisa = 2957.56
		end
		return valor_divisa
	end	
end
