require 'test_helper'

class SpidersControllerTest < ActionController::TestCase
  setup do
    @spider = spiders(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:spiders)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create spider" do
    assert_difference('Spider.count') do
      post :create, spider: { marca_id: @spider.marca_id, path_listado_productos: @spider.path_listado_productos, store_id: @spider.store_id, tipo_id: @spider.tipo_id, url_seccion: @spider.url_seccion }
    end

    assert_redirected_to spider_path(assigns(:spider))
  end

  test "should show spider" do
    get :show, id: @spider
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @spider
    assert_response :success
  end

  test "should update spider" do
    patch :update, id: @spider, spider: { marca_id: @spider.marca_id, path_listado_productos: @spider.path_listado_productos, store_id: @spider.store_id, tipo_id: @spider.tipo_id, url_seccion: @spider.url_seccion }
    assert_redirected_to spider_path(assigns(:spider))
  end

  test "should destroy spider" do
    assert_difference('Spider.count', -1) do
      delete :destroy, id: @spider
    end

    assert_redirected_to spiders_path
  end
end
