class AddConfirmacionToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :confirmacion, :string, :default => ""
    remove_column :purchases, :confirmacion, :string
  end
end
