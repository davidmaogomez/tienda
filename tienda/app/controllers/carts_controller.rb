class CartsController < ApplicationController
  before_action :set_cart, only: [:show, :edit, :update, :destroy]

  # GET /carts
  # GET /carts.json
  def borrar_item
    cart = Cart.find(params[:cart_id])
    if cart
      cart.delete
    end
    redirect_to :back
  end
  def show_cart
    @tipos = Tipo.where(:id => Producto.select(:tipo_id).all)
    @carts = Cart.where.not(:estado_id => 4).where(:user_id => current_user) #busca todas las compras que no se han realizado o han sido fallidas
    @suma = 0
    @carts.each do |cart|
      @suma = @suma + cart.producto.precio_min * cart.quantity
    end    
  end
  def to_cart
    if params[:cart_id].to_i > 0
      c = Cart.find(params[:cart_id])
      if c
        c.quantity = params[:quantity]
        c.save
      end      
    else
      Cart.create(:producto_id => params[:producto_id], :user_id => current_user.id, :quantity => params[:quantity])
    end 
    redirect_to show_cart_path
  end
  def index
    @carts = Cart.all
  end

  # GET /carts/1
  # GET /carts/1.json
  def show
  end

  # GET /carts/new
  def new
    @cart = Cart.new
  end

  # GET /carts/1/edit
  def edit
  end

  # POST /carts
  # POST /carts.json
  def create
    @cart = Cart.new(cart_params)

    respond_to do |format|
      if @cart.save
        format.html { redirect_to @cart, notice: 'Cart was successfully created.' }
        format.json { render :show, status: :created, location: @cart }
      else
        format.html { render :new }
        format.json { render json: @cart.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /carts/1
  # PATCH/PUT /carts/1.json
  def update
    respond_to do |format|
      if @cart.update(cart_params)
        format.html { redirect_to @cart, notice: 'Cart was successfully updated.' }
        format.json { render :show, status: :ok, location: @cart }
      else
        format.html { render :edit }
        format.json { render json: @cart.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /carts/1
  # DELETE /carts/1.json
  def destroy
    @cart.destroy
    respond_to do |format|
      format.html { redirect_to carts_url, notice: 'Cart was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cart
      @cart = Cart.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cart_params
      params.require(:cart).permit(:producto_id, :user_id, :payment_id, :estado_id)
    end
end
