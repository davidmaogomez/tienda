class RenamePageDestalleScrapy < ActiveRecord::Migration
  def change
    rename_column :scrapies, :page_detalle_path, :detalle_path
  end
end
