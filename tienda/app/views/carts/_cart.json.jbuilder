json.extract! cart, :id, :producto_id, :user_id, :payment_id, :estado_id, :created_at, :updated_at
json.url cart_url(cart, format: :json)