package scrapy;
import java.io.IOException;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Main {
    public static void main(String[] args) throws IOException {
        Scrapy scrapy = new Scrapy("http://www.gearbest.com/android-tablet-_gear/c_11294/?on_sale=1");       
        if (scrapy.getStatusConnectionCode(scrapy.url) == 200) {
            Document document = Scrapy.getHtmlDocument(scrapy.url); 
            Elements elementos = Scrapy.getElementsHtmlDocument(document, "div.pro_inner");
            System.out.print(elementos.size());
        }
    }    
}
