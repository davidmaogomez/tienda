module SessionHelper
	def log_in(user)
        session[:user_id] = user.id
    end
    
    def current_user
        @current_user ||= User.find_by(id: session[:user_id])
    end
    
    def logged_in?
        !current_user.nil?
    end
    
    def log_out
        session.delete(:user_id)
        @current_user = nil
    end	

    def permiso(user, nivel)        
        respuesta = false  
        if not user == nil           
            if user.nivel == nivel or user.nivel == 1
                respuesta = true
            end  
        else
            if nivel == 0
                respuesta = true
            else
                respuesta = false
            end
        end   

        if respuesta == false
            redirect_to root_path 
        end
        return respuesta
    end
    
    def compras
    end
    
    def info_compra
        informacion = []
        informacion[0] = Purchase.where(:user_id => current_user.id, :pago => false).count
        purchases = Purchase.where(:user_id => current_user.id, :pago => false)
        valor = 0
        purchases.each do |purchase|
            valor = valor + purchase.valor*purchase.cantidad*purchase.valor_divisa
        end
        informacion[1] = valor
    return informacion
  end
end
