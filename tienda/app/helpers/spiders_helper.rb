module SpidersHelper

	def traer_data_site(ruta_folder, spider)
		resultado = true
		nex_url = ''
		t = Time.now
		ruta_folder = crear_estructura_directorios(ruta_folder, t)
		siguiente = true
		puts "1- ruta_folder #{ruta_folder}"
		spider.path_paginador.split("¬")[1].to_i.times do |time|
			puts "2- entra a path_paginador"
			t = Time.now
			nombre_archivo = "#{ruta_folder}/#{t.strftime("%d_%m_%Y %H_%M_%S")}.html"
			puts "3- siguiente #{siguiente} nombre_archivo #{nombre_archivo}"			
		    File.open(nombre_archivo, 'w') do |file|
		      if spider.path_paginador.split("¬")[2] == 'concatenar' or time == 0
			      if nex_url == ''
			      	url = "#{spider.store.url}#{spider.url_seccion}"
			      else
			      	url = "#{spider.store.url}#{nex_url}"
			      end
			  else		      	
			  	url = "#{nex_url}"
		      end
		      puts "4- url #{url}"	
		      if Net::HTTP.get_response(URI.parse(url)).code == '200'
		      	# puts "4- ***** 200"
		      	contenido = Nokogiri::HTML(open(url))
		      	if contenido.to_s.length > 0
		      		# puts "5- contenido #{contenido.to_s.length}"
			        file.puts contenido
			        nex_url = extract_tag(contenido, spider.path_paginador.split("¬")[0])
			        # puts "-----------nex_url #{nex_url}-----------"
			    else
			        resultado = false		      			      		
			        siguiente = false
			    	# puts "7- resultado #{resultado} siguiente #{siguiente}"			        
		      	end
		        # puts "8- siguiente #{siguiente}"
		      else
		      	resultado = false
		      	# puts "9- resultado #{resultado}"
		      end        
	    	end	
	    	# puts "***************************************************************************************************************************"		
		end
    	return resultado  				
	end

	def crear_estructura_directorios(ruta_folder, t)
	    if File.exist?(ruta_folder) == false
	      Dir.mkdir ruta_folder
	    end    
	    ruta_folder = "#{ruta_folder}/#{t.strftime("%d_%m_%Y %H_%M_%S")}"
	    Dir.mkdir ruta_folder
	    return ruta_folder		
	end

	def extract_tag(node, tag)
		# puts "1---> #{tag}"
		resultado = ""
        if node.at_css(tag.split("|")[0])
        	# puts "2- si at_css #{tag.split("|")[0]} y es #{node.at_css(tag.split("|")[0])}"
        	if tag.split("|")[1] != '' and tag.split("|")[1] != nil
        		resultado = node.css(tag.split("|")[0]).attr(tag.split("|")[1])		
        		# puts "3- #{resultado}"
        	else
	        	resultado = node.css(tag.split("|")[0])		
	        	# puts "4- #{resultado}"
        	end

        	if tag.split("|")[2] == 'text'
        		resultado = resultado.text
        		# puts "5- entro a text #{resultado}"
        	end
        	# puts "extract_tag resultado #{resultado.class}"
        else
        	# puts "no at_css #{tag.split("|")[0]}"
        end
        # puts "------------------------#{resultado}   #{resultado.class}------------------------"
        return resultado
	end

	def extract_info(producto, paths)
		# puts "llego ----> #{paths} cantidad de elemento enviados #{paths.count}"
		resultado = []
		info = ""
		paths.each do |path|
			path.split(",").count.times do |time|
				# puts "Buscamos esto #{path.split(",")[time]}"
				info = "#{extract_tag(producto, path.split(",")[time])}|#{info}"
				# puts "devuelvo #{info} <----"
			end
			resultado.append(info)
		end	
		return resultado
	end
end
