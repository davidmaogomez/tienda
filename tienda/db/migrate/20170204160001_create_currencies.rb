class CreateCurrencies < ActiveRecord::Migration
  def change
    create_table :currencies do |t|
      t.string :name
      t.string :code
      t.float :value

      t.timestamps null: false
    end
    add_reference :productos, :currency, :index => true, :foreign_key => true
    add_reference :spiders, :currency, :index => true, :foreign_key => true
    add_reference :countries, :currency, :index => true, :foreign_key => true
  end
end
