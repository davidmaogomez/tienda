require 'test_helper'

class ProductosControllerTest < ActionController::TestCase
  setup do
    @producto = productos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:productos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create producto" do
    assert_difference('Producto.count') do
      post :create, producto: { descripcion: @producto.descripcion, imagen: @producto.imagen, moneda: @producto.moneda, porcentaje_ganancia: @producto.porcentaje_ganancia, precio_envio: @producto.precio_envio, precio_max: @producto.precio_max, precio_min: @producto.precio_min, precio_venta: @producto.precio_venta, url: @producto.url }
    end

    assert_redirected_to producto_path(assigns(:producto))
  end

  test "should show producto" do
    get :show, id: @producto
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @producto
    assert_response :success
  end

  test "should update producto" do
    patch :update, id: @producto, producto: { descripcion: @producto.descripcion, imagen: @producto.imagen, moneda: @producto.moneda, porcentaje_ganancia: @producto.porcentaje_ganancia, precio_envio: @producto.precio_envio, precio_max: @producto.precio_max, precio_min: @producto.precio_min, precio_venta: @producto.precio_venta, url: @producto.url }
    assert_redirected_to producto_path(assigns(:producto))
  end

  test "should destroy producto" do
    assert_difference('Producto.count', -1) do
      delete :destroy, id: @producto
    end

    assert_redirected_to productos_path
  end
end
