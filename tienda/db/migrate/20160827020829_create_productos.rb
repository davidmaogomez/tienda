class CreateProductos < ActiveRecord::Migration
  def change
    create_table :productos do |t|
      t.string :imagen
      t.text :descripcion
      t.float :precio_min
      t.float :precio_max
      t.float :precio_envio
      t.float :porcentaje_ganancia
      t.string :precio_venta
      t.string :moneda
      t.string :url

      t.timestamps null: false
    end
  end
end
