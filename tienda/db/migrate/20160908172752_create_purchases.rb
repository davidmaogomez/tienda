class CreatePurchases < ActiveRecord::Migration
  def change
    create_table :purchases do |t|
      t.references :user, index: true, foreign_key: true
      t.references :producto, index: true, foreign_key: true
      t.boolean :estado

      t.timestamps null: false
    end
  end
end
