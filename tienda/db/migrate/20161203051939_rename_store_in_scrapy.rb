class RenameStoreInScrapy < ActiveRecord::Migration
  def change  	
  	rename_column :scrapies, :store_id_id, :store_id
  end
end
