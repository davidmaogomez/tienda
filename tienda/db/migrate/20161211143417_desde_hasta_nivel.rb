class DesdeHastaNivel < ActiveRecord::Migration
  def change
  	rename_column :scrapies, :cantidad_niveles, :hasta_nivel
  	add_column :scrapies, :desde_nivel, :integer, :default => 1
  end
end
