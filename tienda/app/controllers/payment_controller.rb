require "uri"
require "net/http"
require 'net/https'
class PaymentController < ApplicationController
  protect_from_forgery with: :null_session 

  def compras
    @tipos = Tipo.where(:id => Producto.select(:tipo_id).all)    
    @carts = Cart.where(:user_id => current_user.id, :estado_id => 4)
  end
  
  def respuesta
    @tipos = Tipo.where(:id => Producto.select(:tipo_id).all)    
    puts "respuesta ********************** Vea los parametros #{params}"
    @parametros = params
    id = params['referenceCode'].to_i - 1000
    @payment = Payment.find(id)
    @payment.resultado = params
    @payment.save
    carts = Cart.where(:payment_id => @payment.id)
    carts.each do |cart|
      cart.estado_id = params['transactionState']
      cart.save        
    end                  
  end
  
  def confirmacion
    puts "Confirmacion ********************** Vea los parametros #{params}"
    @parametros = params
    id = params['reference_sale'].to_i - 1000
    @payment = Payment.find(id)
    @payment.confirmacion = params
    @payment.estado_id = params['state_pol']
    @payment.save
    carts = Cart.where(:payment_id => @payment.id)
    carts.each do |cart|
      cart.estado_id = params['state_pol']
      cart.save        
    end            
  end  

  def create
    constante = 1000
    @carts = Cart.where(:user_id => current_user.id, :estado_id => [1, 5, 6])
    payment = Payment.create(:user_id => current_user.id, :estado_id => 1)
    agrupador = payment.id + constante
    reference_sale = "TestPayU#{agrupador}"
    payment.reference_sale = reference_sale
    @total = 0
    @carts.each do |cart|
      cart.payment_id = payment.id
      cart.save
      @total = @total + (cart.producto.precio_min * cart.quantity)
      payment.valor_compra = @total
      payment.save
    end

    @api_key= '4Vj8eK4rloUd272L48hsrarnUA'
    @merchant_id = '508029'
    @account_id = '512321'
    @reference_code = agrupador# "TestPayU#{agrupador+constante}"
    @amount = @total.to_s #@amont = valor_compra
    @currency_value = 'USD'# @currency_value = 'COP'    
    cadena_combinacion = "#{@api_key}~#{@merchant_id}~#{@reference_code}~#{@amount}~#{@currency_value}"
    @md5_value = Digest::MD5.hexdigest(cadena_combinacion)
    @buyer_email_value = 'damagoza@gmail.com'
    @respuesta_value = "https://tienda-damagoza.c9users.io#{respuesta_path}"
    @confirmacion_value = "https://tienda-damagoza.c9users.io#{confirmacion_path}"
    data = {'api_key': @api_key, 'merchant_id': @merchant_id,  'account_id': @account_id,  'reference_code': @reference_code,  'amount': @amount,  'currency_value': @currency_value,  'adena_combinacion': cadena_combinacion,  'md5_value': @md5_value,  'buyer_email_value': @buyer_email_value,  'respuesta_value': @respuesta_value,  'confirmacion_value': @confirmacion_value}
    # my_connection = Net::HTTP.new('https://sandbox.gateway.payulatam.com/ppp-web-gateway/', 80)
    # reponse = my_connection.post('https://sandbox.gateway.payulatam.com/ppp-web-gateway/', data)
    # puts "response #{response} #{response.class}"
  end

  def mis_compras
    @tipos = Tipo.where(:id => Producto.select(:tipo_id).all)    
    @compras = Payment.where(:user_id => current_user.id)
  end
  
  def admin_ventas
    @tipos = Tipo.where(:id => Producto.select(:tipo_id).all)    
    @compras = Payment.all
    @payments = {}
    Payment.all.group(:agrupador).count.each do |agrupador, cantidad|
      @payments[agrupador] = Payment.where(:agrupador => agrupador)
    end
  end  
end