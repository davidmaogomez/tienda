class CreateScrapies < ActiveRecord::Migration
  def change
    create_table :scrapies do |t|
      t.references :tienda, index: true, foreign_key: true
      t.references :marca, index: true, foreign_key: true
      t.references :tipo, index: true, foreign_key: true
      t.string :imagen_path
      t.string :descripcion_path
      t.string :precio_path
      t.string :moneda_path
      t.string :url_path
      t.string :page_detalle_path
      t.string :detalle_title_path
      t.string :detalle_des_path
      t.string :detalle_general_path
      t.string :detail_gallery_path
      t.string :imagenes_path
      t.string :imag_path

      t.timestamps null: false
    end
  end
end
