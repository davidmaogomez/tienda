class AddDefaultIva < ActiveRecord::Migration
  def change
  	change_column :productos, :iva, :float, :default => 0
  end
end
