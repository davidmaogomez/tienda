class AddPrecioSinIvaGrv < ActiveRecord::Migration
  def change
    add_column :productos, :precio_sin_iva_grv, :float, :default => 0
    add_column :productos, :precio_anterior, :float, :default => 0
  end
end
