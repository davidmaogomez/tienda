class AddAgrupadorToPurchase < ActiveRecord::Migration
  def change
    add_column :purchases, :agrupador, :integer, :default => 0
  end
end
