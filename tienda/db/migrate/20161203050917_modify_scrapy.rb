class ModifyScrapy < ActiveRecord::Migration
  def change
  	remove_column :scrapies, :tienda_id, :integer
  	add_reference :scrapies, :store_id, index: true, foreign_key: true
  end
end
