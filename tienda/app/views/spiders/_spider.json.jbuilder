json.extract! spider, :id, :store_id, :marca_id, :tipo_id, :url_seccion, :path_listado_productos, :created_at, :updated_at
json.url spider_url(spider, format: :json)