class AddObjectSiteHtmlAndPathsToSpider < ActiveRecord::Migration
  def change
  	add_column :spiders, :site_html, :string, :default => ''
	add_column :spiders, :path_producto_description, :string, :default => ''
    add_column :spiders, :path_producto_referencia, :string, :default => ''
    add_column :spiders, :path_producto_precio, :string, :default => ''
    add_column :spiders, :path_producto_imagen_principal, :string, :default => ''
    add_column :spiders, :path_producto_url_detalle, :string, :default => ''
    add_column :spiders, :path_producto_imagenes, :string, :default => ''
    add_column :spiders, :path_producto_detalle_description, :string, :default => ''
    add_column :spiders, :path_producto_detalle_referencia, :string, :default => ''
  end
end
