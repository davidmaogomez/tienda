json.extract! marca, :id, :nombre, :url_imagen, :url, :created_at, :updated_at
json.url marca_url(marca, format: :json)