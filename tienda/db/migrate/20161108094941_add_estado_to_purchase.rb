class AddEstadoToPurchase < ActiveRecord::Migration
  def change
    add_reference :purchases, :estado, index: true, foreign_key: true, :default => true
  end
end
