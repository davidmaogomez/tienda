require 'test_helper'

class DataControllerTest < ActionController::TestCase
  setup do
    @datum = data(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:data)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create datum" do
    assert_difference('Datum.count') do
      post :create, datum: { marca_id: @datum.marca_id, path_listado_productos: @datum.path_listado_productos, path_paginator: @datum.path_paginator, path_producto_description: @datum.path_producto_description, path_producto_detalle_description: @datum.path_producto_detalle_description, path_producto_detalle_referencia: @datum.path_producto_detalle_referencia, path_producto_imagen_principal: @datum.path_producto_imagen_principal, path_producto_imagenes: @datum.path_producto_imagenes, path_producto_imagenes: @datum.path_producto_imagenes, path_producto_precio: @datum.path_producto_precio, path_producto_referencia: @datum.path_producto_referencia, path_producto_url_detalle: @datum.path_producto_url_detalle, store_id: @datum.store_id, tipo_id: @datum.tipo_id, url_section: @datum.url_section }
    end

    assert_redirected_to datum_path(assigns(:datum))
  end

  test "should show datum" do
    get :show, id: @datum
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @datum
    assert_response :success
  end

  test "should update datum" do
    patch :update, id: @datum, datum: { marca_id: @datum.marca_id, path_listado_productos: @datum.path_listado_productos, path_paginator: @datum.path_paginator, path_producto_description: @datum.path_producto_description, path_producto_detalle_description: @datum.path_producto_detalle_description, path_producto_detalle_referencia: @datum.path_producto_detalle_referencia, path_producto_imagen_principal: @datum.path_producto_imagen_principal, path_producto_imagenes: @datum.path_producto_imagenes, path_producto_imagenes: @datum.path_producto_imagenes, path_producto_precio: @datum.path_producto_precio, path_producto_referencia: @datum.path_producto_referencia, path_producto_url_detalle: @datum.path_producto_url_detalle, store_id: @datum.store_id, tipo_id: @datum.tipo_id, url_section: @datum.url_section }
    assert_redirected_to datum_path(assigns(:datum))
  end

  test "should destroy datum" do
    assert_difference('Datum.count', -1) do
      delete :destroy, id: @datum
    end

    assert_redirected_to data_path
  end
end
