module ProductosHelper
	def productos_tag
	    productos_tag = Producto.all    
	    gon.searchProducto = [""]
	    cont = 0
	    productos_tag.each do |producto|
	      cont += 1
	      gon.searchProducto[cont] = producto.descripcion
	    end      	
	    return productos_tag	
	end

	def diccionario
		diccionario = {}
	    diccionario['Desbloqueador de teléfonos']='Desbloqueado'
	    diccionario['Definición de grabación']='Grabaciòn'
	    diccionario['Color de visualización']='Color de visualización'
	    diccionario['Nombre de la marca']='Marca'
	    diccionario['Espesor']='Espesor'
	    diccionario['Diseño']='Espesor'
	    diccionario['UPC']='Procesador'
	    diccionario['Teléfono móvil']='Móvil'
	    diccionario['Resolución de la pantalla']='Resolución'
	    diccionario['Tipo de Pantalla Táctil']='Pantalla'
	    diccionario['Google Play']='Google Play'
	    diccionario['Cantidad de Tarjetas SIM']='SIM'
	    diccionario['Fecha de lanzamiento']='Lanzamiento'
	    diccionario['RAM']='Ram'
	    diccionario['Modo de la venda']='Tipo de SIM'
	    diccionario['Fabricante del procesador']='Fabricante del procesador'
	    diccionario['Cámara']='Cámara'
	    diccionario['Tiempo de Conversación']='Tiempo de Conversación'
	    diccionario['ROM']='ROM'
	    diccionario['Funcionamiento del sistema']='Funcionamiento del sistema'
	    diccionario['Característica']='Característica'
	    diccionario['Tipo de batería']='Tipo de batería'
	    diccionario['Modelo de Xiaomi']='Modelo'
	    diccionario['Condición del artículo']='Condición del artículo'
	    diccionario['Capacidad de la batería (mAh)']='Batería (mAh)'
	    diccionario['Tipo de la cámara']='Tipo de la cámara'
	    diccionario['Idioma']='Idioma'
	    diccionario['Tamaño']='Tamaño'
	    diccionario['Tamaño de la pantalla']='Tamaño pantalla'
	    diccionario['CPU Model']='Procesador'
	    diccionario['GPU']='GPU'
	    diccionario['Screen']='Screen'
	    diccionario[' TF Card  ']='TF Card'
	    diccionario['front camera']='Cámara frontal'
	    diccionario['wireless connect']='Wireless connect'
	    diccionario['Fingerprint ID']='Fingerprint ID'
	    diccionario['Network']='Network'
	    diccionario['VIDEO']='VIDEO'
	    diccionario['charging power']='charging power'
	    return diccionario 		
	end
	def cantidad_de_productos
		return Producto.all.count
	end
	def cantidad_productos_marca()
		cantidades = []
		Marca.all.each do |m|
			cantidades[m.id] = Productos.where(:marca_id => m.id).count
		end
		return cantidades
	end
	def price(producto, precio_solicitado, numero_de_veces)
		return number_to_currency((precio_solicitado*numero_de_veces*producto.divisa).round, precision: 0).to_s.gsub(/[,]/, '.')
	end
end
