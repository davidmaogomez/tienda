class User < ActiveRecord::Base
  belongs_to :city
  has_many :purchases
  has_many :payments
  has_many :carts
end
