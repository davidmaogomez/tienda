require 'open-uri'
require 'rubygems'
require 'nokogiri'
require 'net/http'

user_agent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7"
urls = {}
urls['tipo'] = 1
xiaomi = {}
xiaomi['identificador'] = 1
xiaomi['octacore'] = 'http://es.aliexpress.com/af/category/204006047.html?d=n&isViewCP=y&CatId=204006047&catName=tel-fonos-m-viles&spm=2114.04020108.0.33.vBqkIH&isrefine=y&site=esp&isBrandWall=y&pvId=200001044-200658763%2C2-200658765&origin=n'
xiaomi['quadcore'] = 'https://es.aliexpress.com/category/204006047/mobile-phones.html?spm=2114.04010108.0.0.zxUzHi&isrefine=y&site=esp&isBrandWall=y&pvId=200001044-200005417,2-200658765'	
xiaomi['una-sim'] = 'https://es.aliexpress.com/category/204006047/mobile-phones.html?spm=2114.04010108.0.0.7AQEb3&isrefine=y&site=esp&isBrandWall=y&pvId=200000791-200003798,2-200658765'
xiaomi['dos-sim'] = 'https://es.aliexpress.com/category/204006047/mobile-phones.html?spm=2114.04010108.0.0.32v6NQ&isrefine=y&site=esp&isBrandWall=y&pvId=200000791-200003799,2-200658765'

urls['xiaomi'] = xiaomi



File.open("tienda/db/seeds.rb", "w") do |file|
iteraciones = 0
	urls.each do |elementos_key, elementos_value|
		if elementos_key != 'tipo'
			elementos_value.each do |elemento_key, url|
				if elemento_key != "identificador"
					if url != ''
						uri	= URI.parse(url)				
						response = Net::HTTP.get_response(uri)
						if response.code == '200'
							puts "#{elemento_key} OK"
							url_site = url
							html_data_site = open(url_site)
							# puts "---> #{html_data_site}"
							object_site_html = Nokogiri::HTML(html_data_site)			
							productos = object_site_html.css('ul.util-clearfix.lazy-load li')
							cont = 0
									
							productos.each do |producto|
								tipo = 1	
								if "#{producto.css('div.img a img').attr('src')}".length > 0
									imagen = "#{producto.css('div.img a img').attr('src')}".gsub(/[\n]/, '')
								end
								if "#{producto.css('div.img a img').attr('image-src')}".length > 0
									imagen = "#{producto.css('div.img a img').attr('image-src')}".gsub(/[\n]/, '')
								end
								descripcion = producto.css('div.detail a').text.gsub(/\/5.'/, "")	
								descripcion = descripcion.gsub(/'/, "")							
								precio = producto.css('div.info.infoprice span.price.price-m span.value')
								precio_min = precio.text.split('-').first.gsub(/€/, "").gsub(/\s+/, "").to_f
								precio_max = precio.text.split('-').last.gsub(/\s+/, "").to_f
								precio_envio = producto.css('div.info.infoprice dl.pnl-shipping dd.price span.value').text.split(' ').last.to_f
								porcentaje_ganancia = 0.05.to_f
	
								if precio_max == 0.0
									precio_max = precio_min
								end		
								precio_base = precio_max +  precio_envio	
								iva = precio_base * 0.19.to_f
								grv = (precio_envio + iva) * 0.05.to_f
								comision_plataforma = precio_base * 0.03.to_f
								ganancia = precio_base * 0.05.to_f
								precio_venta = precio_base + iva + grv + comision_plataforma + ganancia
								precio_sin_iva_grv = precio_base + comision_plataforma + ganancia
	
	
								moneda = precio.text[0].gsub(/[\n]/, '')
								url = producto.css('div.detail a').attr('href')
								
								url = "http:#{url}"
								# puts "descripcion --> #{descripcion}"
								# puts "precio_min --> #{precio_min}"
								# puts "precio_max --> #{precio_max}"
								# puts "precio_envio --> #{precio_envio}"
								# puts "porcentaje_ganancia --> #{porcentaje_ganancia}"
								# puts "ganancia --> #{ganancia}"
								# puts "precio_venta --> #{precio_venta}"
								# puts "moneda --> #{moneda}"
								# puts "url #{url}"
								
								page_detalle = Nokogiri::HTML(open(url, 'User-Agent' => user_agent).read)
								detalle = page_detalle.css('ul.product-property-list.util-clearfix li.property-item')
								detalle_general = ""	
								detalle.each do |detalle_especifico|
									detalle_title = detalle_especifico.css('span.propery-title').text
									detalle_des = detalle_especifico.css('span.propery-des').text				
									detalle_general += "#{detalle_title} #{detalle_des} |"
								end
	
								detail_gallery = page_detalle.css('div.detail-gallery')
								imagenes = detail_gallery.css('span.img-thumb-item')
								if imagenes.count > 0
									imagenes.each do |bloque|
										imag = ""
										if "#{bloque.css('img').attr('src')}".length > 0
											imag = "#{bloque.css('img').attr('src')}".gsub(/[\n]/, '')
										end	
										if "#{bloque.css('img').attr('image-src')}".length > 0
											imag = "#{bloque.css('img').attr('src')}".gsub(/[\n]/, '')
										end							
										if imag != ""
											imagen += "|#{imag}"							
										end
									end
								else
									#puts url
								end
								imagen += "|"							
								# puts "imagen --> #{imagen}"
								# puts detalle_general
								cont += 1	
								cadena = "if Producto.where('descripcion LIKE ?', '#{descripcion}%').count == 0\n"\
										 "	p = Producto.create(:imagen => '#{imagen}', :descripcion => '#{descripcion}', :precio_min => #{precio_min}, :precio_max => #{precio_max}, :precio_envio => #{precio_envio}, :porcentaje_ganancia => #{porcentaje_ganancia}, :precio_venta => #{precio_venta}, :precio_sin_iva_grv => #{precio_sin_iva_grv}, :moneda => '#{moneda}', :url => '#{url}', :marca_id => #{elementos_value['identificador']}, :detalle => '#{detalle_general}', :grv => #{grv}, :iva => #{iva}, :ganancia => #{ganancia}, :precio_base => #{precio_base}, :tipo_id => #{urls['tipo']})\n"\
 										 "	Change.create(:producto_id => p.id, :precio => p.precio_venta)\n"\
										 "	puts 'Producto nuevo'\n"\
										 "else\n"\
										 "	p = Producto.find_by_descripcion('#{descripcion}')\n"\
										 "	if p\n"\
										 "		p.nuevo=false\n"\
										 "		p.imagen='#{imagen}'\n"\
										 "		p.precio_sin_iva_grv=#{precio_sin_iva_grv}\n"\
										 "		p.save\n"\
										 "		if p.precio_venta != #{precio_venta}\n"\
										 "			puts 'cambio de precio '\n"\
										 "			p.cambio_precio=true\n"\
										 "			p.cambio_precio_porcentaje=((#{precio_venta}*100)/p.precio_venta)-100\n"\
										 "			p.precio_min=#{precio_min}\n"\
										 "			p.precio_max=#{precio_max}\n"\
										 "			p.precio_envio=#{precio_envio}\n"\
										 "			p.grv=#{grv}\n"\
										 "			p.iva=#{iva}\n"\
										 "			p.ganancia=#{ganancia}\n"\
										 "			p.precio_base=#{precio_base}\n"\
										 "			p.precio_sin_iva_grv=#{precio_sin_iva_grv}\n"\
										 "			p.precio_anterior=p.precio_venta\n"\
										 "			p.precio_venta=#{precio_venta}\n"\
										 "			p.save\n"\
 										 "			Change.create(:producto_id => p.id, :precio => p.precio_venta)\n"\
										 "		else\n"\
										 "			p.cambio_precio=false\n"\
										 "			p.cambio_precio_porcentaje=0\n"\
										 "			p.save\n"\
										 "		end\n"\
										 "	else\n"\
										 "	end\n"\
										 "end\n"
								# puts cadena
								file.puts cadena
								# puts "----------"
							end
							# puts "********************"
							
							# puts "Productos encontrados #{cont}"									
						else
							puts "#{elemento_key} #{response.code}"						
							if response.code == '302'
								response = Net::HTTP.get_response(URI.parse(response.header['location']))
								puts "	reintento #{response.code}"
							end
						end					
	
					end
				end
				sleep 10
			end			
		end
		puts "#{elementos_key}"
		iteraciones = iteraciones + 1 
		puts "------------------------------------------------------------------------------------------------------------------"
	end
end
#davidmao de local a nube