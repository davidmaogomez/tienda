class RenameColumnToSPider < ActiveRecord::Migration
  def change
     rename_column :spiders, :path_productos, :path_precio_detalle
  end
end
