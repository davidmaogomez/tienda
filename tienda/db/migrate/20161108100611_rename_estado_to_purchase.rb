class RenameEstadoToPurchase < ActiveRecord::Migration
  def change
    rename_column :purchases, :estado, :pago
  end
end
