class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  # GET /users.json
  def panel_admin
    @tipos = Tipo.where(:id => Producto.select(:tipo_id).all)    
  end  
  def panel_user
    @tipos = Tipo.where(:id => Producto.select(:tipo_id).all)    
  end
  def my_perfil
    
  end
  def index
    permiso(current_user, 1)
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
    permiso(current_user, 1)         
  end

  # GET /users/new
  def new  
    @tipos = Tipo.where(:id => Producto.select(:tipo_id).all)  
    permiso(current_user, 0)
    @user = User.new    
  end

  # GET /users/1/edit
  def edit    
    @tipos = Tipo.where(:id => Producto.select(:tipo_id).all)    
    if current_user.id == params[:id].to_i
      permiso(current_user, 3)
    else
      redirect_to root_path
    end  
  end

  # POST /users
  # POST /users.json
  def create
    permiso(current_user, 1)
    if User.where(:email => user_params[:email]).count == 0
      @user = User.new(user_params)
    
      if @user.save
        log_in(@user)
        #redirect_to root_path
      else
        #redirect_to new_user_path
      end      
    else
      #redirect_to new_user_path
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    if logged_in?
      if @user.update(user_params)
        redirect_to root_path
      else
        
      end      
    
    end    
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:user_name, :email, :clave, :city_id, :addres, :birthday, :nivel)
    end
end
