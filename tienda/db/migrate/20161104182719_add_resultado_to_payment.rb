class AddResultadoToPayment < ActiveRecord::Migration
  def change
    add_column :payments, :resultado, :string, :default => ""
  end
end
