class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.references :purchase, index: true, foreign_key: true
      t.integer :agrupador

      t.timestamps null: false
    end
  end
end
