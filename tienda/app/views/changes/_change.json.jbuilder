json.extract! change, :id, :producto_id, :precio, :created_at, :updated_at
json.url change_url(change, format: :json)