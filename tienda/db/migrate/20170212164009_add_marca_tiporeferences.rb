class AddMarcaTiporeferences < ActiveRecord::Migration
  def change
	add_reference :spiders, :marca_tipo, index: true, foreign_key: true  	
  end
end
