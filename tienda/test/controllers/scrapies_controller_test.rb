require 'test_helper'

class ScrapiesControllerTest < ActionController::TestCase
  setup do
    @scrapy = scrapies(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:scrapies)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create scrapy" do
    assert_difference('Scrapy.count') do
      post :create, scrapy: { descripcion_path: @scrapy.descripcion_path, detail_gallery_path: @scrapy.detail_gallery_path, detalle_des_path: @scrapy.detalle_des_path, detalle_general_path: @scrapy.detalle_general_path, detalle_title_path: @scrapy.detalle_title_path, imag_path: @scrapy.imag_path, imagen_path: @scrapy.imagen_path, imagenes_path: @scrapy.imagenes_path, marca_id: @scrapy.marca_id, moneda_path: @scrapy.moneda_path, page_detalle_path: @scrapy.page_detalle_path, precio_path: @scrapy.precio_path, tienda_id: @scrapy.tienda_id, tipo_id: @scrapy.tipo_id, url_path: @scrapy.url_path }
    end

    assert_redirected_to scrapy_path(assigns(:scrapy))
  end

  test "should show scrapy" do
    get :show, id: @scrapy
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @scrapy
    assert_response :success
  end

  test "should update scrapy" do
    patch :update, id: @scrapy, scrapy: { descripcion_path: @scrapy.descripcion_path, detail_gallery_path: @scrapy.detail_gallery_path, detalle_des_path: @scrapy.detalle_des_path, detalle_general_path: @scrapy.detalle_general_path, detalle_title_path: @scrapy.detalle_title_path, imag_path: @scrapy.imag_path, imagen_path: @scrapy.imagen_path, imagenes_path: @scrapy.imagenes_path, marca_id: @scrapy.marca_id, moneda_path: @scrapy.moneda_path, page_detalle_path: @scrapy.page_detalle_path, precio_path: @scrapy.precio_path, tienda_id: @scrapy.tienda_id, tipo_id: @scrapy.tipo_id, url_path: @scrapy.url_path }
    assert_redirected_to scrapy_path(assigns(:scrapy))
  end

  test "should destroy scrapy" do
    assert_difference('Scrapy.count', -1) do
      delete :destroy, id: @scrapy
    end

    assert_redirected_to scrapies_path
  end
end
