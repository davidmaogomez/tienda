Rails.application.routes.draw do
  resources :carts
  resources :currencies
  resources :spiders
  resources :data
  resources :scrapies
  resources :stores
  resources :estados
  resources :changes
  

  resources :tipos
  get 'session/new'

  resources :purchases
  resources :users
  resources :cities
  resources :countries
  resources :marcas
  resources :productos
  root 'productos#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
  get 'find_producto' => 'productos#find_producto'
  get 'productos_por_marca_tipo' => 'productos#productos_por_marca_tipo'
  get 'add_to_card/:producto_id' => 'purchases#add_to_card', as: :add_to_card
  get 'show_card' => 'purchases#show_card'
  get 'session/new'
  get    'login'   => 'session#new'
  post   'login'   => 'session#create'
  delete 'logout'  => 'session#destroy'  
  get 'my_perfil' => 'users#my_perfil'
  get 'crear' => 'payment#create', as: :crear
  get "respuesta" => 'payment#respuesta', as: :respuesta
  post "confirmacion" => 'payment#confirmacion', as: :confirmacion
  #get "compras" => 'purchases#mis_compras', as: :compras
  get "panel_user" => 'users#panel_user', as: :panel_user
  get "panel_admin" => 'purchases#panel_admin', as: :panel_admin
  get "admin_ventas" => 'payment#admin_ventas', as: :admin_ventas
  get 'crear_pago' => 'purchases#crear_pago', as: :crear_pago
  get 'extract/:id' => 'spiders#extract', as: :extract
  get 'traer_data/:id' => 'spiders#traer_data', as: :traer_data
  post 'asignar_spider/origen/destino' => 'spiders#asignar_spider', as: :asignar_spider
  get 'productos_por_tipo' => 'productos#productos_por_tipo'  
  get 'productos_por_marca' => 'productos#productos_por_marca'  
  post 'to_cart/producto_id/quantity/cart_id' => 'carts#to_cart', as: :to_cart
  get 'show_cart' => 'carts#show_cart'
  get 'borrar_item' => 'carts#borrar_item', as: :borrar_item
  get 'compras' => 'payment#compras', as: :compras
end
