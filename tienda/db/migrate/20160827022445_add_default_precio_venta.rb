class AddDefaultPrecioVenta < ActiveRecord::Migration
  def change
  	change_column :productos, :precio_venta, :float, :default => 0
  end
end
