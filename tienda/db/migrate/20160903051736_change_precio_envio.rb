class ChangePrecioEnvio < ActiveRecord::Migration
  def change
  	change_column :productos, :precio_envio, :float, :default => 0
  end
end
