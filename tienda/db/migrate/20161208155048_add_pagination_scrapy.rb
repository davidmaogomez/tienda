class AddPaginationScrapy < ActiveRecord::Migration
  def change
  	add_column :scrapies, :next_page_path, :string, :default => ''
  end
end
