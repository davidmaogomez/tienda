require 'open-uri'
require 'net/http'
class ScrapiesController < ApplicationController
  before_action :set_scrapy, only: [:show, :edit, :update, :destroy]

  # GET /scrapies
  # GET /scrapies.json
  def extract
    user_agent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7"
    @seguimiento = 0
    @scrapy = Scrapy.find(params[:id])
    uri = URI.parse("#{@scrapy.dominio}/#{@scrapy.url}")        
    response = Net::HTTP.get_response(uri)
    if response.code == '200'
      html_data_site = open("#{@scrapy.dominio}#{@scrapy.url}")
      object_site_html = Nokogiri::HTML(html_data_site)
      extract_items(object_site_html, @scrapy)      
    else
      @seguimiento = "no abrio la url raiz"
      if response.code == '302'
      end
    end
  end


  def index
    permiso(current_user, 1)
    @scrapies = Scrapy.all
  end

  # GET /scrapies/1
  # GET /scrapies/1.json
  def show
    permiso(current_user, 1)
  end

  # GET /scrapies/new
  def new
    permiso(current_user, 1)
    @scrapy = Scrapy.new
  end

  # GET /scrapies/1/edit
  def edit
    permiso(current_user, 1)    
  end

  # POST /scrapies
  # POST /scrapies.json
  def create
    permiso(current_user, 1)
    @scrapy = Scrapy.new(scrapy_params)

    respond_to do |format|
      if @scrapy.save
        format.html { redirect_to @scrapy, notice: 'Scrapy was successfully created.' }
        format.json { render :show, status: :created, location: @scrapy }
      else
        format.html { render :new }
        format.json { render json: @scrapy.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /scrapies/1
  # PATCH/PUT /scrapies/1.json
  def update
    permiso(current_user, 1)
    respond_to do |format|
      if @scrapy.update(scrapy_params)
        format.html { redirect_to @scrapy, notice: 'Scrapy was successfully updated.' }
        format.json { render :show, status: :ok, location: @scrapy }
      else
        format.html { render :edit }
        format.json { render json: @scrapy.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /scrapies/1
  # DELETE /scrapies/1.json
  def destroy
    permiso(current_user, 1)
    @scrapy.destroy
    respond_to do |format|
      format.html { redirect_to scrapies_url, notice: 'Scrapy was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_scrapy
      @scrapy = Scrapy.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def scrapy_params
      params.require(:scrapy).permit(:store_id, :marca_id, :tipo_id, :imagen_path, :descripcion_path, :precio_path, :moneda_path, :url_path, :detalle_path, :detalle_title_path, :detalle_des_path, :detalle_general_path, :detail_gallery_path, :imagenes_path, :imag_path, :url, :productos_path, :imagen_atributos, :url_atributos, :imag_atributos, :next_page_path, :dominio, :hasta_nivel, :desde_nivel)
    end
end
