class CreateChanges < ActiveRecord::Migration
  def change
    create_table :changes do |t|
      t.references :producto, index: true, foreign_key: true
      t.float :precio

      t.timestamps null: false
    end
  end
end
