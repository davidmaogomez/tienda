class MarcaTipo < ActiveRecord::Base
  belongs_to :tipo
  belongs_to :marca
  has_many :productos
  has_many :spiders
end
