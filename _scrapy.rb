require 'open-uri'
require 'rubygems'
require 'nokogiri'
require 'open_uri_redirections'
require 'open_uri_w_redirect_to_https'
require 'httparty'
#require 'openssl'

#OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
user_agent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7"
urls = []
# xiaomi
#urls[0] = 'http://es.aliexpress.com/af/category/204006047.html?d=n&isViewCP=y&CatId=204006047&catName=tel-fonos-m-viles&spm=2114.04020108.0.33.vBqkIH&isrefine=y&site=esp&isBrandWall=y&pvId=200001044-200658763%2C2-200658765&origin=n'
# # samsung
# urls[1] = 'https://es.aliexpress.com/af/category/204006047.html?d=n&isViewCP=y&CatId=204006047&catName=tel-fonos-m-viles&spm=2114.04020108.0.37.tDU8yi&isrefine=y&site=esp&isBrandWall=y&pvId=200001044-200658763%2C2-100007876&origin=n'
# # huawei
# urls[2] = 'https://es.aliexpress.com/af/category/204006047.html?d=n&isViewCP=y&CatId=204006047&catName=tel-fonos-m-viles&spm=2114.04020108.0.34.Y9Mhsn&site=esp&isrefine=y&isBrandWall=y&pvId=2-200003854&origin=n'
# # apple
# # urls[3] = 'https://es.aliexpress.com/af/category/204006047.html?d=n&isViewCP=y&CatId=204006047&catName=tel-fonos-m-viles&spm=2114.04020108.0.39.Y9Mhsn&site=esp&isrefine=y&isBrandWall=y&pvId=2-3426&origin=n' # problemas
# urls[3] = ""
# # # zte
# urls[4] = 'https://es.aliexpress.com/af/category/204006047.html?d=n&isViewCP=y&CatId=204006047&catName=tel-fonos-m-viles&spm=2114.04020108.0.40.Y9Mhsn&site=esp&isrefine=y&isBrandWall=y&pvId=2-200005314&origin=n'
# # # oneplus
# urls[5] = 'https://es.aliexpress.com/af/category/204006047.html?d=n&isViewCP=y&CatId=204006047&catName=tel-fonos-m-viles&spm=2114.30011008.0.40.Z0Sg1z&isAffiliate=y&isrefine=y&site=esp&isBrandWall=y&pvId=2-201299258&origin=n'
# # # lg
# urls[6] = 'https://es.aliexpress.com/af/category/204006047.html?d=n&isViewCP=y&CatId=204006047&catName=tel-fonos-m-viles&spm=2114.04020108.0.51.Y9Mhsn&site=esp&isrefine=y&isBrandWall=y&pvId=2-361795&origin=n'
urls[0] = 'https://es.aliexpress.com/category/204006047/mobile-phones.html?spm=2114.04010108.0.32.50zKnX&site=esp&isrefine=y&isBrandWall=y&pvId=2-200658765'
# samsung
urls[1] = 'https://es.aliexpress.com/category/204006047/mobile-phones.html?spm=2114.04010108.0.35.VIbQNe&isrefine=y&site=esp&isBrandWall=y&pvId=2-100007876'
# huawei
urls[2] = 'https://es.aliexpress.com/category/204006047/mobile-phones.html?spm=2114.04010108.0.33.VIbQNe&isrefine=y&site=esp&isBrandWall=y&pvId=2-200003854'
# apple
# urls[3] = 'https://es.aliexpress.com/af/category/204006047.html?d=n&isViewCP=y&CatId=204006047&catName=tel-fonos-m-viles&spm=2114.04020108.0.39.Y9Mhsn&site=esp&isrefine=y&isBrandWall=y&pvId=2-3426&origin=n' # problemas
urls[3] = ""
# zte
urls[4] = 'https://es.aliexpress.com/category/204006047/mobile-phones.html?spm=2114.04010108.0.39.VIbQNe&isrefine=y&site=esp&isBrandWall=y&pvId=2-200005314'
# # oneplus
urls[5] = 'https://es.aliexpress.com/category/204006047/mobile-phones.html?spm=2114.04010108.0.41.VIbQNe&isrefine=y&site=esp&isBrandWall=y&pvId=2-201299258'
# # lg
urls[6] = 'https://es.aliexpress.com/category/204006047/mobile-phones.html?spm=2114.04010108.0.50.VIbQNe&isrefine=y&site=esp&isBrandWall=y&pvId=2-361795'
File.open("tienda/db/seeds.rb", "w") do |file|
iteraciones = 0
	urls.each do |url|

		if url != ''
			page = HTTParty.get(url)
			puts page
			object_site_html = Nokogiri::HTML(page)

			# url_site = url
			# html_data_site = open(url_site).read
			# object_site_html = Nokogiri::HTML(html_data_site)			
			productos = object_site_html.css('ul.util-clearfix.lazy-load li')
			cont = 0
					
			productos.each do |producto|
				tipo = 1	
				if "#{producto.css('div.img a img').attr('src')}".length > 0
					imagen = "#{producto.css('div.img a img').attr('src')}".gsub(/[\n]/, '')
				end
				if "#{producto.css('div.img a img').attr('image-src')}".length > 0
					imagen = "#{producto.css('div.img a img').attr('image-src')}".gsub(/[\n]/, '')
				end
				descripcion = producto.css('div.detail a').text.gsub(/\/5.'/, "")
				precio = producto.css('div.info.infoprice span.price.price-m span.value')
				precio_min = precio.text.split('-').first.gsub(/€/, "").gsub(/\s+/, "").to_f
				precio_max = precio.text.split('-').last.gsub(/\s+/, "").to_f
				precio_envio = producto.css('div.info.infoprice dl.pnl-shipping dd.price span.value').text.split(' ').last.to_f
				porcentaje_ganancia = 0.05.to_f

				if precio_max == 0.0
					precio_max = precio_min
				end		
				precio_base = precio_max +  precio_envio	
				iva = precio_base * 0.19.to_f
				grv = (precio_envio + iva) * 0.05.to_f
				comision_plataforma = precio_base * 0.03.to_f
				ganancia = precio_base * 0.05.to_f
				precio_venta = precio_base + iva + grv + comision_plataforma + ganancia
				precio_sin_iva_grv = precio_base + comision_plataforma + ganancia


				moneda = precio.text[0].gsub(/[\n]/, '')
				url = producto.css('div.detail a').attr('href')
				
				url = "http:#{url}"
				puts "descripcion --> #{descripcion}"
				puts "precio_min --> #{precio_min}"
				puts "precio_max --> #{precio_max}"
				puts "precio_envio --> #{precio_envio}"
				puts "porcentaje_ganancia --> #{porcentaje_ganancia}"
				puts "ganancia --> #{ganancia}"
				puts "precio_venta --> #{precio_venta}"
				puts "moneda --> #{moneda}"
				puts "url #{url}"
				
				page_detalle = Nokogiri::HTML(open(url, 'User-Agent' => user_agent).read)
				detalle = page_detalle.css('ul.product-property-list.util-clearfix li.property-item')
				detalle_general = ""	
				detalle.each do |detalle_especifico|
					detalle_title = detalle_especifico.css('span.propery-title').text
					detalle_des = detalle_especifico.css('span.propery-des').text				
					detalle_general += "#{detalle_title} #{detalle_des} |"
				end

				detail_gallery = page_detalle.css('div.detail-gallery')
				imagenes = detail_gallery.css('span.img-thumb-item')
				if imagenes.count > 0
					imagenes.each do |bloque|
						imag = ""
						if "#{bloque.css('img').attr('src')}".length > 0
							imag = "#{bloque.css('img').attr('src')}".gsub(/[\n]/, '')
						end	
						if "#{bloque.css('img').attr('image-src')}".length > 0
							imag = "#{bloque.css('img').attr('src')}".gsub(/[\n]/, '')
						end							
						if imag != ""
							imagen += "|#{imag}"							
						end
					end
				else
					#puts url
				end
				imagen += "|"							
				puts "imagen --> #{imagen}"
				puts detalle_general
				cont += 1	
				cadena = "if Producto.where('descripcion LIKE ?', '#{descripcion}%').count == 0\n 	Producto.create(:imagen => '#{imagen}', :descripcion => '#{descripcion}', :precio_min => #{precio_min}, :precio_max => #{precio_max}, :precio_envio => #{precio_envio}, :porcentaje_ganancia => #{porcentaje_ganancia}, :precio_venta => #{precio_venta}, :precio_sin_iva_grv => #{precio_sin_iva_grv}, :moneda => '#{moneda}', :url => '#{url}', :marca_id => #{iteraciones+1}, :detalle => '#{detalle_general}', :grv => #{grv}, :iva => #{iva}, :ganancia => #{ganancia}, :precio_base => #{precio_base}, :tipo_id => 1) \nelse \n   p = Producto.find_by_descripcion('#{descripcion}')\n   p.nuevo=false\n   p.imagen='#{imagen}'\n   p.precio_sin_iva_grv=#{precio_sin_iva_grv}\n   p.save\n   if p.precio_venta != #{precio_venta}\n      puts 'cambio de precio '\n      p.cambio_precio=true\n      p.cambio_precio_porcentaje=((#{precio_venta}*100)/p.precio_venta)-100\n      p.precio_min=#{precio_min}\n      p.precio_max=#{precio_max}\n      p.precio_envio=#{precio_envio}\n      p.grv=#{grv}\n      p.iva=#{iva}\n      p.ganancia=#{ganancia}\n      p.precio_base=#{precio_base}\n      p.precio_sin_iva_grv=#{precio_sin_iva_grv}\n      p.precio_anterior=p.precio_venta\n      p.precio_venta=#{precio_venta}\n      p.save\n   else\n      p.cambio_precio=false\n      p.cambio_precio_porcentaje=0\n   end\nend"
				puts cadena
				file.puts cadena
				puts "----------"
			end
			#puts "********************"
			
			puts "Productos encontrados #{cont}"			
		end
		iteraciones = iteraciones + 1 
	end
end
#davidmao de local a nube