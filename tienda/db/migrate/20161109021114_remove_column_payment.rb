class RemoveColumnPayment < ActiveRecord::Migration
  def change
    remove_column :payments, :purchase_id, :integer
  end
end
