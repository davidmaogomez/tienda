require_relative 'scrapy.rb'
class	Ali
	def initialize()
		@urls = {}
		@urls['tipo'] = 1
		@urls['xiaomi'] = 'http://es.aliexpress.com/af/category/204006047.html?d=n&isViewCP=y&CatId=204006047&catName=tel-fonos-m-viles&spm=2114.04020108.0.33.vBqkIH&isrefine=y&site=esp&isBrandWall=y&pvId=200001044-200658763%2C2-200658765&origin=n'
		# @urls['samsung'] = 'https://es.aliexpress.com/af/category/204006047.html?d=n&isViewCP=y&CatId=204006047&catName=tel-fonos-m-viles&spm=2114.04020108.0.37.tDU8yi&isrefine=y&site=esp&isBrandWall=y&pvId=200001044-200658763%2C2-100007876&origin=n'
		# @urls['huawei'] = 'https://es.aliexpress.com/af/category/204006047.html?d=n&isViewCP=y&CatId=204006047&catName=tel-fonos-m-viles&spm=2114.04020108.0.34.Y9Mhsn&site=esp&isrefine=y&isBrandWall=y&pvId=2-200003854&origin=n'			
		# @urls['apple'] = 'https://es.aliexpress.com/af/category/204006047.html?d=n&isViewCP=y&CatId=204006047&catName=tel-fonos-m-viles&spm=2114.04020108.0.39.Y9Mhsn&site=esp&isrefine=y&isBrandWall=y&pvId=2-3426&origin=n' # problemas
		# @urls['zte'] = 'https://es.aliexpress.com/af/category/204006047.html?d=n&isViewCP=y&CatId=204006047&catName=tel-fonos-m-viles&spm=2114.04020108.0.40.Y9Mhsn&site=esp&isrefine=y&isBrandWall=y&pvId=2-200005314&origin=n'
		# @urls['oneplus'] = 'https://es.aliexpress.com/af/category/204006047.html?d=n&isViewCP=y&CatId=204006047&catName=tel-fonos-m-viles&spm=2114.30011008.0.40.Z0Sg1z&isAffiliate=y&isrefine=y&site=esp&isBrandWall=y&pvId=2-201299258&origin=n'						
		# @urls['lg'] = 'https://es.aliexpress.com/af/category/204006047.html?d=n&isViewCP=y&CatId=204006047&catName=tel-fonos-m-viles&spm=2114.04020108.0.51.Y9Mhsn&site=esp&isrefine=y&isBrandWall=y&pvId=2-361795&origin=n'								
	end

	def urls
		return @urls
	end
end

ali = Ali.new()
iteraciones = 0
ali.urls.each do |name, url|
	puts name
	if name != 'tipo'
		scrapy_producto = Scrapy.new(name, url)
		productos = scrapy_producto.get_element(scrapy_producto.get_html_object, 'ul.util-clearfix.lazy-load li')
		productos.each do |producto|	
			elemento_imagen = scrapy_producto.get_element(producto, 'div.img a img')
			url_imagen = scrapy_producto.get_atribute(elemento_imagen, 'src', 'image-src') # ready. antes imagen
			descripcion = scrapy_producto.get_element(producto, 'div.detail a').text.gsub(/\/5.'/, "") # ready.
			precio = scrapy_producto.get_element(producto, 'div.info.infoprice span.price.price-m span.value')
			precio_min = precio.text.split('-').first.gsub(/€/, "").gsub(/\s+/, "").to_f 
			precio_max = precio.text.split('-').last.gsub(/\s+/, "").to_f 
			precio_envio = scrapy_producto.get_element(producto, 'div.info.infoprice dl.pnl-shipping dd.price span.value').text.split(' ').last.to_f
			moneda = precio.text[0].gsub(/[\n]/, '')
			url = scrapy_producto.get_atribute(scrapy_producto.get_element(producto, 'div.detail a'), 'href', '')

			scrapy_producto_datalle = Scrapy.new(descripcion, url)
			detalle = scrapy_producto_datalle.get_element(scrapy_producto_datalle.get_html_object, 'ul.product-property-list.util-clearfix li.property-item')



			porcentaje_ganancia = 0.05.to_f
			if precio_max == 0.0
				precio_max = precio_min
			end		
			precio_base = precio_max +  precio_envio	
			iva = precio_base * 0.19.to_f
			grv = (precio_envio + iva) * 0.05.to_f
			comision_plataforma = precio_base * 0.03.to_f
			ganancia = precio_base * 0.05.to_f
			precio_venta = precio_base + iva + grv + comision_plataforma + ganancia
			precio_sin_iva_grv = precio_base + comision_plataforma + ganancia
			puts detalle		
			puts "-----------------------"
		end
	end
end

