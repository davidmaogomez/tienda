class AddreferenceSaleToPayment < ActiveRecord::Migration
  def change
    add_column :payments, :reference_sale, :string
  end
end
